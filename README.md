# PyPI project repo

A cookiecutter template for python packages stored in gitlab repos.

The philosophy behind this template is, "it's easier to delete things than to create them". Accordingly, this template includes more files than you'll likely need:

1. Files for releasing your package on PyPI
2. A gitlab CI/CD pipeline
3. A makefile for autoformatting, linting, typehinting, unit testing, and documenting
4. Files for creating virtual environments (with `venv`, `virtualenv`, or `conda`)
5. Files for running code in cloud environments (binder and gitpod)

## Quickstart

First, install [cookiecutter](https://cookiecutter.readthedocs.io/en/1.7.2/).

```
$ pip install cookiecutter
```

Use this template directly from the repo:

```
$ cookiecutter https://gitlab.com/dsbowen/pypi-template
```

Or clone the repo, modify the template, and use locally:

```
$ git clone https://gitlab.com/dsbowen/pypi-template
$ cookiecutter pypi-template
```

## Folder structure

- `docs/` Sphinx docs
- `examples/` Example notebooks and other files
- `src/` Package source code (this is where the magic happens)
- `temp/` For storing temporary notebooks and other files
- `tests/` Unit tests
- `.gitignore`
- `.gitlab-ci.yml` CI/CD file
- `.gitpod.yml` Configures gitpod prebuilt environment
- `LICENSE` MIT License
- `Makefile` Defines commands for autoformatting, unit testing, and code quality checks
- `MANIFEST.in` For including package data (not included, but you may want to write one)
- `README.md`
- `environment.yml` Setup for conda environment
- `postBuild` Runs script after binder build (by default this installs an editable version of the package)
- `pyproject.toml` For release on PyPI
- `requirements.txt` Used by venv, (possibly) conda environment, binder, gitpod, and CI/CD services
- `runtime.txt` Sets the python version, used by binder, gitpod, deployment services like heroku
- `setup.cfg` For release on PyPI
- `setup.py` Required for editable installs
- `tox.ini` Configures automated unit tests

## Set up your environment

With `virtualenv`:

```bash
$ python venv venv
$ . venv/bin/activate  # . venv/scripts/activate on windows
$ pip install -r requirements.txt
$ pip install ipykernel
```

With `conda`:

```bash
$ conda env create -f environment.yml
$ conda activate <my-environment-name>
$ pip install -r requirements.txt
```

I recommend using `requirements.txt` even with conda because the `requirements.txt` is used by other frameworks that don't support `environment.yml` (gitpod, heroku, CI/CD services, etc.).

To use the package in `src`, for example in your `examples` directory, install an editable version of your package with:

```bash
$ pip install -e .
```

## Pipeline

This template implements a pipeline which runs autoformatting, unit tests, linting, and doc creation and testing. Run the pipeline with:

```bash
$ make pipeline
```

This generates reports (e.g., about code quality) in `reports/` and docs in `docs/_build`.

`.gitlab-ci.yml` implements a similar CI pipeline that runs each time you push to the gitlab repo.

The pipeline is modular. Below, I provide `make` commands for running individual nodes of the pipeline.

Modify `Makefile` to tailor the pipeline. `.gitlab-ci.yml` calls `make` commands, so any changes to the makefile will also be made to your CI pipeline. Don't forget to add third-party packages to `requirements.txt` if you need them to run part of the pipeline.

### Format

Run just the formatting node of the pipeline with:

```bash
$ make format
```

`black` is beautiful, but `yapf` seems great too.

### Test

Run unit tests and generate a coverage report with `make test`. Run `make testserve` and go to [http://localhost:8080/] to see the coverage report (for the last tox environment). Open `reports/test.txt` to view the unit test report. (Run `tox` to run unit tests without the coverage report).

Note that my version of `.gitlab-ci.yml` only tests against the python version you specified when your ran the `cookiecutter` command. To test against other environments, create a new job in `.gitlab-ci.yml` that looks something like this:

```yml
test-py38-job:
    extends: test-job
    image: python:3.8
    variables:
        - TOXENV: py38
```

### Lint

Run linting with `make lint`. Open `reports/lint.txt` to view the code quality report.

### Typehint

Run type-hinting with `make typehint`. Open `reports/typehint.txt` to view the type-hinting report.

### Docs

Run `make docs` to both create and test sphinx docs. Run `make docserve` to serve your docs on [http://localhost:8020/]. Open `reports/output.txt` to view the doctest report.

Tips:

1. I use Sphinx's [setuptools integration](https://www.sphinx-doc.org/en/master/usage/advanced/setuptools.html) to keep all the configuration variables in one place (`setup.cfg`).
2. After using Sphinx's `autogen`, go into the stub `.rst` files and add the `:members:` option to the `automodules`. This tells Sphinx autodoc to automatically document all the functions and classes from your python modules.
3. Put [doctests](https://www.sphinx-doc.org/en/master/usage/extensions/doctest.html) in your docstrings. `make docs` automatically runs Sphinx's doctests to check the example code in your docstrings against your code's actual behavior.

I think of doctests as additional unit tests. Accordingly, I built the `.gitlab-ci.yml` so that your pipeline will fail if your doctests fail.

## In-browser dev environments

This template includes a [binder](https://mybinder.org/) badge for a sandbox in-browser environment. This is the perfect solution for showcasing examples or allowing data scientists to reproduce your analysis without setting up their own environment. Opening jupyter lab instead of jupyter notebook is as simple as adding `?urlpath=lab` to the notebook URL binder gives you by default. [See here](https://mybinder.readthedocs.io/en/latest/howto/user_interface.html#jupyterlab) for specific instructions on converting notebook URLs to lab URLs.

[Gitpod](https://gitpod.io/) is a great cloud-based IDE if a lightweight binder doesn't cut it. I haven't tried promising alternatives such as [Codeanywhere](https://codeanywhere.com/) and [Red Hat CodeReady](https://developers.redhat.com/products/codeready-workspaces/overview). Edit `.gitpod.yml` to customize your prebuilt workspace.

Tips:

1. One downside of gitpod is that you can't work with notebooks in its IDE. Fortunately, there's an easy workaround. Install jupyter in the workspace and open jupyter from the terminal with `jupyter lab --NotebookApp.allow_origin=\'$(gp url 8888)\'`.

## PyPI

This template includes files (specifically `setup.cfg`, `pyproject.toml`, and `LICENSE`) so you can easily release your package on PyPI. Tutorial [here](https://packaging.python.org/tutorials/packaging-projects/).

Tips:

1. Don't forget to add the package requirements to `install_requires` in `setup.cfg`
2. If your package needs non-`.py` files, include paths to those in a file in your root directory named `MANIFEST.in`.

## Acknowledgements

Much inspiration for this template drawn from the [original cookiecutter template](https://github.com/asweigart/cookiecutter-basicpythonproject).