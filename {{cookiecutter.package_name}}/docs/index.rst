.. {{ cookiecutter.project_name }} documentation main file, created by
   sphinx-quickstart on Mon Nov 12 14:17:27 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

{{cookiecutter.project_name}} documentation
===========================================

{{ cookiecutter.project_short_description }}

.. image:: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.gitlab_repo }}/badges/main/pipeline.svg
   :target: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.gitlab_repo }}/-/commits/main
.. image:: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.gitlab_repo }}/badges/main/coverage.svg
   :target: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.gitlab_repo }}/-/commits/main
.. image:: https://badge.fury.io/py/{{ cookiecutter.package_name }}.svg
   :target: https://badge.fury.io/py/{{ cookiecutter.package_name }}
.. image:: https://img.shields.io/badge/License-MIT-brightgreen.svg
   :target: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.gitlab_repo }}/-/blob/main/LICENSE
.. image:: https://mybinder.org/badge_logo.svg
   :target: https://mybinder.org/v2/gl/{{ cookiecutter.gitlab_username }}%2F{{ cookiecutter.gitlab_repo }}/HEAD?urlpath=lab
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black

Installation
============

.. code-block:: console

   $ pip install {{ cookiecutter.package_name }}

Quickstart
==========

TODO

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Changelog <changelog>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Citations
=========

.. code-block::

   {% set name = cookiecutter.author_name.split() %}
   @software({{ name[-1].lower() }}{% now 'utc', '%Y' %}{{ cookiecutter.package_name.split()[0].lower() }},
      title={ {{ cookiecutter.project_name }} },
      author={ {{ name[-1]}}, {{ " ".join(name[:-1]) }} },
      year={ {% now 'utc', '%Y' %} },
      url={ https://{{ cookiecutter.gitlab_username}}.gitlab.io/{{ cookiecutter.gitlab_repo}} }
   )

Acknowledgements
================

TODO