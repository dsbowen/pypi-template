# {{ cookiecutter.project_name }}

[![pipeline status](https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.gitlab_repo }}/badges/main/pipeline.svg)](https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.gitlab_repo }}/-/commits/main)
[![coverage report](https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.gitlab_repo }}/badges/main/coverage.svg)](https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.gitlab_repo }}/-/commits/main)
[![PyPI version](https://badge.fury.io/py/{{ cookiecutter.package_name }}.svg)](https://badge.fury.io/py/{{ cookiecutter.package_name }})
[![License](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.gitlab_repo }}/-/blob/main/LICENSE)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/{{ cookiecutter.gitlab_username }}%2F{{ cookiecutter.gitlab_repo }}/HEAD?urlpath=lab/tree/examples)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

{{ cookiecutter.project_short_description }} [Read the docs](https://{{ cookiecutter.gitlab_username }}.gitlab.io/{{ cookiecutter.gitlab_repo }}).
